#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <fstream>
#include <filesystem>

namespace fs = std::filesystem;
using namespace cv;
using namespace std;

bool checkOneImage(string checkDir, string groundDir, string imageFilename, bool doImageShow) {

	// Create full paths
	string checkPath = checkDir + "/" + imageFilename;
	string groundPath = groundDir + "/" + imageFilename;

	// Loading images...
	//cout << "Loading image to check: " << checkPath << endl;
	Mat checkImage = imread(checkPath, IMREAD_GRAYSCALE);
	if (!checkImage.data) {
		cerr << "ERROR: Could not open image " << checkPath << endl;
	}

	//cout << "Loading ground image: " << groundPath << endl;
	Mat groundImage = imread(groundPath, IMREAD_GRAYSCALE);
	if (!groundImage.data) {
		cerr << "ERROR: Could not open image " << groundPath << endl;
	}

	// Convert to int images
	Mat checkInt, groundInt;
	checkImage.convertTo(checkInt, CV_32SC1);
	groundImage.convertTo(groundInt, CV_32SC1);

	// Get difference image
	Mat diffImage = cv::abs(checkImage - groundImage);
	int sumDiff = cv::sum(cv::abs(diffImage))[0];

	cout << imageFilename << " difference: " << sumDiff << endl;

	// Show images
	if (doImageShow) {
		imshow("Ground:" + imageFilename, groundImage);
		imshow("Check:" + imageFilename, checkImage);
		imshow("Diff:" + imageFilename, diffImage);
		waitKey(-1);
		destroyAllWindows();
	}

	return (sumDiff == 0);
}

int main(int argc, char **argv) {

	// There should be THREE command line parameters
	if (argc < 4) {
		cerr << "ERROR: Must pass in <ground directory> <check directory> <1 = imshow?>" << endl;
		exit(1);
	}

	// Get paths from command line arguments
	string groundDir = string(argv[1]);
	string checkDir = string(argv[2]);	
	bool doImageShow = argv[3][0] == '1';

	// Loop through every file (ASSUMES THAT EVERYTHING IN THERE IS AN IMAGE)
	for (const auto& file : fs::directory_iterator(groundDir)) {
		string imageFilename = file.path().filename().string();
		if (!checkOneImage(checkDir, groundDir, imageFilename, doImageShow)) {
			cout << "*****************************" << endl;
			cout << "WARNING: ERROR NON-ZERO!!!!" << endl;
			cout << "*****************************" << endl;
		}
	}
		
	return 0;
}


